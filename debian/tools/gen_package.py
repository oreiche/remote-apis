#!/usr/bin/env python3

import os
import re
import sys
from pathlib import Path
from typing import Dict, List, Set

INPUTDIR = "$(INPUT)"
BUILDDIR = "$(BUILDDIR)"

INCLUDE = ["build"]
EXCLUDE = []
DEV_DEPS = [
    "libprotobuf-dev", "libgrpc++-dev", "libgoogle-api-dev",
    "libgoogle-longrunning-dev", "libgoogle-rpc-dev"
]

LIBDIR = "lib/$(DEB_HOST_MULTIARCH)"
INCDIR = "include"
SO_SUFFIX = ".2.0.0"  # suffix of the actual library file, e.g., .1.2.3
SO_NAME_SUFFIX = ".2.0"  # suffix of the library soname, e.g., .1.2


def _path_to_module_uid(path: str) -> str:
    """Heuristic for module separation. Maps a single proto path to unique
    module UID (e.g., path).

    Approach: Produce single module 'remote.apis' containing all protos
    """
    return "remote.apis"


def _uid_to_module_name(uid: str) -> str:
    """Compute module name from UID via this (non-bijective) function."""
    return uid.replace('.', '_')


class Module:
    def __init__(self, uid: str, deps: Set[str], protos: Set[str]):
        self.uid = uid
        self.name = _uid_to_module_name(self.uid)
        self.deps = [_uid_to_module_name(uid) for uid in sorted(deps)]
        self.__protos = sorted(protos)

    def __derived(self, ext: str) -> List[str]:
        files: List[str] = []
        for p in [Path(p) for p in self.__protos]:
            base = f"{p.parent}/{p.stem}"
            files += [f"{base}.pb{ext}", f"{base}.grpc.pb{ext}"]
        return files

    def stat_lib_name(self) -> str:
        return f"lib{self.name}.a"

    def shrd_lib_name(self) -> str:
        return f"lib{self.name}.so{SO_SUFFIX}"

    def shrd_lib_soname(self) -> str:
        return f"lib{self.name}.so{SO_NAME_SUFFIX}"

    def objs(self) -> List[str]:
        return self.__derived(".o")

    def hdrs(self) -> List[str]:
        return self.__derived(".h")

    def symlinks(self) -> List[str]:  # includes soname
        links: List[str] = []
        libname = Path(self.shrd_lib_name())
        for link in [libname, Path(self.shrd_lib_soname())]:
            while link.suffix:
                if (link != libname) and (str(link) not in links):
                    links.append(str(link))
                link = Path(link.stem)
        return links

    def symlinks_dev(self) -> List[str]:  # filters out soname
        return [l for l in self.symlinks() if l != self.shrd_lib_soname()]


def _read_from_proto(path: str, pattern: str):
    with open(path) as f:
        for line in f.readlines():
            match = re.search(pattern, line)
            if match: yield match


def _detect_cycle(modules: Dict[str, Module]):
    stack: List[str] = []
    clean: Set[str] = set()

    def visit(name: str):
        if name in clean: return
        if name in stack: raise Exception(f"-> found cyclic dep to {name}")
        stack.append(name)
        try:
            [visit(dep) for dep in modules[name].deps]
        except Exception as ex:
            raise Exception(f"| called from {name}\n{ex}")
        stack.pop()

    try:
        [visit(mod) for mod in modules.keys()]
    except Exception as ex:
        print(f"error: dependency cycle detected\n{ex}")
        exit(1)


def gen_modules() -> Dict[str, Module]:
    all_protos: Set[str] = set()
    protos_by_module: Dict[str, Set[str]] = dict()
    modules_by_name: Dict[str, Module] = dict()

    for p in INCLUDE:
        for root, _, files in os.walk(p):
            if True in {root.startswith(e) for e in EXCLUDE}: continue
            for file_name in files:
                path = Path(root, file_name)
                if path.suffix == ".proto":
                    all_protos.add(str(path))
                    uid = _path_to_module_uid(str(path))
                    protos_by_module.setdefault(uid, set()).add(str(path))

    for mod_uid, protos in protos_by_module.items():
        dep_uids: Set[str] = set()
        for proto in protos:
            for match in _read_from_proto(proto, '^import\\s+"(.+)"'):
                if match.group(1) in all_protos:
                    dep_uid = _path_to_module_uid(match.group(1))
                    if dep_uid != mod_uid: dep_uids.add(dep_uid)
        module = Module(mod_uid, dep_uids, protos)
        if modules_by_name.get(module.name):  # check name's uniqueness
            print("error: module name clash between " +
                  f"'{module.uid}' and '{modules_by_name[module.name].uid}'")
            exit(1)
        modules_by_name[module.name] = module

    _detect_cycle(modules_by_name)

    return dict(sorted(modules_by_name.items()))


def _gen_stat_lib_target(module: Module) -> str:
    dep_objs = " ".join([f"{BUILDDIR}/{o}" for o in module.objs()])
    return "\n".join([
        "", f"{BUILDDIR}/{module.stat_lib_name()}: {dep_objs}",
        f"\t$(AR) cqs $@ $^", ""
    ])


def _gen_shrd_lib_target(module: Module, modules: Dict[str, Module]) -> str:
    lib_names = [modules[d].shrd_lib_name() for d in module.deps]
    ldflags = f"$(LDFLAGS) -Wl,-soname,{module.shrd_lib_soname()} "
    ldflags += f"-L{BUILDDIR} "
    ldflags += " ".join([f"-l:{d}" for d in lib_names])
    dep_libs = " ".join([f"{BUILDDIR}/{d}" for d in lib_names])
    dep_objs = " ".join([f"{BUILDDIR}/{o}" for o in module.objs()])
    return "\n".join([
        "", f"{BUILDDIR}/{module.shrd_lib_name()}: {dep_objs} {dep_libs}",
        f"\t$(CXX) $(filter %.o,$^) {ldflags} -o $@", ""
    ])


def _gen_libs_target(modules: Dict[str, Module]) -> str:
    libs: List[str] = []
    for module in modules.values():
        libs.append(f"{BUILDDIR}/{module.stat_lib_name()}")
        libs.append(f"{BUILDDIR}/{module.shrd_lib_name()}")
    return "\n".join(["", f"libs: {' '.join(libs)}", ""])


def _gen_install_cmds(module: Module) -> List[str]:
    libdir = f"$(DESTDIR)/$(PREFIX)/{LIBDIR}"
    incdir = f"$(DESTDIR)/$(PREFIX)/{INCDIR}"
    stat_lib = module.stat_lib_name()
    shrd_lib = module.shrd_lib_name()
    link_inst = [
        f"\t ln -s {shrd_lib} {libdir}/{l}" for l in module.symlinks()
    ]
    hdrs_inst = [
        f"\t install -D {INPUTDIR}/{h} {incdir}/{h}" for h in module.hdrs()
    ]
    return [
        f"\t install -D $(BUILDDIR)/{stat_lib} {libdir}/{stat_lib}",
        f"\t install -D $(BUILDDIR)/{shrd_lib} {libdir}/{shrd_lib}"
    ] + link_inst + hdrs_inst + [
        f"\t $(file >{BUILDDIR}/{module.name}.pc,$(call GEN_PC_FILE,{module.name},{' '.join(module.deps)}))",
        f"\t install -D {BUILDDIR}/{module.name}.pc {libdir}/pkgconfig/{module.name}.pc"
    ]


def _gen_install_target(modules: Dict[str, Module]) -> str:
    lines = ["", "install: libs"]
    for module in modules.values():
        lines += _gen_install_cmds(module)
    return "\n".join(lines + [""])


def gen_makefile(in_file: Path, modules: Dict[str, Module]):
    with open(in_file, 'r') as template:
        data = template.readlines()
        for module in modules.values():
            data += _gen_stat_lib_target(module)
            data += _gen_shrd_lib_target(module, modules)
        data += _gen_libs_target(modules)
        data += _gen_install_target(modules)
        with open(f"{in_file.parent}/{in_file.stem}", 'w') as f:
            f.writelines(data)


def _gen_package(m: Module) -> str:
    name = m.name.replace('_', '-')
    deps = ",".join([f"lib{d.replace('_','-')}" for d in m.deps])
    dev_deps = ",".join([f"lib{name}"] + DEV_DEPS +
                        [f"lib{d.replace('_', '-')}-dev" for d in m.deps])
    return f"""
Package: lib{name}
Architecture: any
Multi-Arch: same
Depends: ${{shlibs:Depends}}, ${{misc:Depends}}, {deps}
Description: C++ bindings for google APIs
 This package contains

Package: lib{name}-dev
Architecture: any
Multi-Arch: same
Depends: ${{misc:Depends}}, {dev_deps}
Description: C++ headers for google APIs
 This package contains
"""


def gen_control(in_file: Path, modules: Dict[str, Module]):
    with open(in_file, 'r') as template:
        data = template.readlines()
        data += [_gen_package(m) for m in modules.values()]
        with open(f"{in_file.parent}/{in_file.stem}", 'w') as f:
            f.writelines(data)


def _gen_install_file(module: Module) -> str:
    return "\n".join({
        f"/usr/lib/*/{module.shrd_lib_name()}",
        f"/usr/lib/*/{module.shrd_lib_soname()}"
    })


def _gen_dev_install_file(module: Module) -> str:
    return "\n".join([f"/usr/lib/*/pkgconfig/{module.name}.pc"] +
                     [f"/usr/lib/*/{module.stat_lib_name()}"] +
                     [f"/usr/lib/*/{l}" for l in module.symlinks_dev()] +
                     [f"/usr/include/{h}" for h in module.hdrs()])


def gen_install_files(modules: Dict[str, Module]):
    for m in modules.values():
        name = m.name.replace('_', '-')
        with open(f"debian/lib{name}.install", 'w') as f:
            f.write(_gen_install_file(m))
        with open(f"debian/lib{name}-dev.install", 'w') as f:
            f.write(_gen_dev_install_file(m))


if __name__ == "__main__":
    in_mkfile = sys.argv[1] if len(
        sys.argv) > 1 else "debian/cpp_apis.makefile.in"
    in_control = sys.argv[2] if len(sys.argv) > 2 else "debian/control.in"
    modules = gen_modules()
    [print(f"  {m.name}: {m.deps}") for m in modules.values()]
    print(f"status: found {len(modules)} modules")
    gen_makefile(Path(in_mkfile), modules)
    gen_control(Path(in_control), modules)
    gen_install_files(modules)
