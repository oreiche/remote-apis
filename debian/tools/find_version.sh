#!/bin/sh
#
# Find the googleapis commit that is compatible with system's protoc,
# i.e., look for the first commit that satisfies condition:
#   PiperId(googleapis) <= PiperId(protoc)

set -eu

TMPDIR=$(mktemp -d)
PROTOC_VERSION=$(protoc --version | cut -d' ' -f2)

cd $TMPDIR

git init -q
git fetch -q https://github.com/protocolbuffers/protobuf v${PROTOC_VERSION}

i=0
while [ true ]; do
  PROTOC_ID=$(git log -n1 FETCH_HEAD~$i | grep PiperOrigin-RevId | cut -d':' -f2)
  if [ -n "${PROTOC_ID}" ]; then
    break
  fi
  i=$(($i+1))
done

cd -
rm -r $TMPDIR

echo "PROTOC_ID:${PROTOC_ID}"

git fetch -q https://github.com/googleapis/googleapis master
git branch -f upstream FETCH_HEAD

i=0
while [ true ]; do
  APIS_ID=$(git log -n1 upstream/master~$i | grep PiperOrigin-RevId | cut -d':' -f2)
  if [ -n "${APIS_ID}" ]; then
    if [ ${APIS_ID} -le ${PROTOC_ID} ]; then
      echo "${APIS_ID} <=${PROTOC_ID}: $(git log -n1 --format=%H upstream/master~$i)"
      break
    fi
    echo "${APIS_ID} >${PROTOC_ID}"
  fi
  i=$(($i+1))
done

