CXXDEPS:= google_api google_longrunning google_rpc
CXXDEPS_CXXFLAGS:= $(shell pkg-config --cflags $(CXXDEPS))
CXXDEPS_LDFLAGS:= $(shell pkg-config --libs $(CXXDEPS))

INPUT ?= ./debian/gen/cpp
BUILDDIR ?= ./debian/build/cpp
AR ?= ar
CXX ?= c++
CXXFLAGS ?= -O2 -DNDEBUG
LDFLAGS ?=

CXXFLAGS += -I$(INPUT) -fPIC $(CXXDEPS_CXXFLAGS)
LDFLAGS += -shared $(CXXDEPS_LDFLAGS)

define GEN_PC_FILE
prefix=$(PREFIX)
exec_prefix=$${prefix}
includedir=$${prefix}/include
libdir=$${exec_prefix}/lib/$(DEB_HOST_MULTIARCH)

Name: Google APIs library '${1}'
Description: C++ bindings for Google APIs library '${1}'
Version: 0
Cflags: -I$${includedir}
Requires: ${2} $(CXXDEPS)
Libs: -L$${libdir} -l${1}
Libs.private:
endef

all: libs

$(BUILDDIR)/%.o: $(INPUT)/%.cc
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILDDIR)/libremote_apis.a: $(BUILDDIR)/build/bazel/remote/execution/v2/remote_execution.pb.o $(BUILDDIR)/build/bazel/remote/execution/v2/remote_execution.grpc.pb.o $(BUILDDIR)/build/bazel/semver/semver.pb.o $(BUILDDIR)/build/bazel/semver/semver.grpc.pb.o
	$(AR) cqs $@ $^

$(BUILDDIR)/libremote_apis.so.2.0.0: $(BUILDDIR)/build/bazel/remote/execution/v2/remote_execution.pb.o $(BUILDDIR)/build/bazel/remote/execution/v2/remote_execution.grpc.pb.o $(BUILDDIR)/build/bazel/semver/semver.pb.o $(BUILDDIR)/build/bazel/semver/semver.grpc.pb.o 
	$(CXX) $(filter %.o,$^) $(LDFLAGS) -Wl,-soname,libremote_apis.so.2.0 -L$(BUILDDIR)  -o $@

libs: $(BUILDDIR)/libremote_apis.a $(BUILDDIR)/libremote_apis.so.2.0.0

install: libs
	 install -D $(BUILDDIR)/libremote_apis.a $(DESTDIR)/$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/libremote_apis.a
	 install -D $(BUILDDIR)/libremote_apis.so.2.0.0 $(DESTDIR)/$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/libremote_apis.so.2.0.0
	 ln -s libremote_apis.so.2.0.0 $(DESTDIR)/$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/libremote_apis.so.2.0
	 ln -s libremote_apis.so.2.0.0 $(DESTDIR)/$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/libremote_apis.so.2
	 ln -s libremote_apis.so.2.0.0 $(DESTDIR)/$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/libremote_apis.so
	 install -D $(INPUT)/build/bazel/remote/execution/v2/remote_execution.pb.h $(DESTDIR)/$(PREFIX)/include/build/bazel/remote/execution/v2/remote_execution.pb.h
	 install -D $(INPUT)/build/bazel/remote/execution/v2/remote_execution.grpc.pb.h $(DESTDIR)/$(PREFIX)/include/build/bazel/remote/execution/v2/remote_execution.grpc.pb.h
	 install -D $(INPUT)/build/bazel/semver/semver.pb.h $(DESTDIR)/$(PREFIX)/include/build/bazel/semver/semver.pb.h
	 install -D $(INPUT)/build/bazel/semver/semver.grpc.pb.h $(DESTDIR)/$(PREFIX)/include/build/bazel/semver/semver.grpc.pb.h
	 $(file >$(BUILDDIR)/remote_apis.pc,$(call GEN_PC_FILE,remote_apis,))
	 install -D $(BUILDDIR)/remote_apis.pc $(DESTDIR)/$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/remote_apis.pc
